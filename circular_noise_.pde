import processing.svg.*;

int npoints = 1800;
int maxLayers = 80;

boolean record = true;

void setup() {
  //fullScreen();
  size(800, 800);
  noCursor();

  if (!record) {
    //frameRate(30);
    background(0);
    stroke(255);
  }
  strokeWeight(.1);
}

void draw() {
  if (record) {
    beginRecord(SVG, "layers/layer-####.svg");
  }  
  perimeter();

  if (record) {
    //soul();
    endRecord();
  }
  if (frameCount > maxLayers) {
    saveFrame("final");
    exit();
  }
}

void perimeter() {

  randomSeed(13);
  float angle = TWO_PI/npoints;
  float radius = 300;
  float t = 0.02* frameCount;
  noFill();
  beginShape();
  for (int e = 0; e <= npoints; e++) {
    float x = cos(angle*e)*radius;
    float y = sin(angle*e)*radius;
    PVector p = (new PVector(x, y)).normalize();
    float s = noise(p.x + t, p.y + t);
    float n = map(s, 0, 1, 30, 220);

    p.mult(n*2);
    vertex(width/2 + p.x, height/2 + p.y);
  }
  endShape();
}
void soul() {
  noFill();
  ellipse(width/2-45, height/2, 80, 80);
  ellipse(width/2+45, height/2, 80, 80);
}
